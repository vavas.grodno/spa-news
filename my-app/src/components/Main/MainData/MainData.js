import React from 'react';
import { NavLink } from 'react-router-dom';
import NewsDataBase from '../../NewsDataBase/NewsDataBase';
import classes from '../MainData/MainData.sass';

const MainData = () => {
    return (
        <div className='mainData'>
            <h1 className='mainDataTitle'>
                Всегда <br></br>свежие <span>новости</span> 
            </h1>

            <div className='mainDataBlock'>
                <NewsDataBase
                    countOfNews={6}
                />
            </div>

            <div className='mainDataInfo'>
                <NavLink
                    exact
                    to='/News'
                >
                    Быть в курсе событий
                </NavLink>

            </div>
        </div>
    )
}

export default MainData;