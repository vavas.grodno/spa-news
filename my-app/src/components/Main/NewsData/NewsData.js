import React from 'react';
import {withRouter} from 'react-router-dom';
import classes from '../NewsData/NewsData.sass';


const NewsData = props => {

    const now = new Date(props.date);
    const theMonth = now.getMonth() + 1;
    const theDate = now.getDate();

    const addZero = (param) =>{
        if (param < 10) {
            param = '0' + param;
        };
        return param;
    }

    const finishDate = addZero(theDate)
    const finishMonth = addZero(theMonth)

    return (
        <div className='NewsData'>
            <div className='NewsDataBlock'>
                <div className='newsBlockTitle'
                onClick={()=> {
                    props.history.push('/News/' + props.author, [props.title, props.content, props.date, props.image, props.author, props.url])
                }}>
                    <h3 className='newsBlockTitle'>
                        {props.title}
                    </h3>
                </div>

                <div className='newsBlockInfo'>
                    <div className='newsBlockInfoLink'>
                        <a href={props.url} target="_blank">
                            {props.author}
                        </a>

                    </div>
                    
                    <div className='newsBlockInfoDate'>
                        <div className='myDate'>{finishDate}</div>
                         / 
                         <div className='myMonth'>{finishMonth}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withRouter(NewsData);