import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {Route, NavLink} from 'react-router-dom';
import ContactsData from './ContactsData/ContactsData';
import MainData from './MainData/MainData';
import NewsList from '../NewsList/NewsList';
import NewsDetail from '../NewsDataBase/NewsDetail/NewsDetail';



const Main = () => {

    return (

        <div className='main'>

            <Route
                path='/'
                exact
                component={MainData}
            />

            <Route
                path='/News'
                exact  
                component={NewsList}
            />

            <Route
                path='/Contacts'
                exact
                component={ContactsData}
            />

            <Route
                path='/News/:title'
                exact
                component={NewsDetail}
            />            
        </div>
    )
}

export default Main;