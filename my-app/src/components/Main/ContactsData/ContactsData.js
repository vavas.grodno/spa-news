import React from 'react';
import foto from './img/foto_vavas.JPG'
import classes from '../ContactsData/ContactsData.sass'

const ContactsData = () => {
    return (
        <div className='contactsData'>
            <div className='contactsDataInfo'>
                <a title="Позвонить" href="tel:+375297891192" className='contactsDataPhone'>+375 (29) 789 11 92</a>
                
                <p className='contactsDataTitle'>
                    Хобец <br></br> Владимир
                </p>
                
                <a title="mail" href="mailto:vavas-terran@mail.ru" className='contactsDataMail'>vavas-terran@mail.ru</a>
                
                <p className='contactsDataProf'>
                    JavaScript разработчик
                </p>

                <p className='contactsDataList'>
                ES5, ES6,<span> React</span> 
                </p>
            </div>

            <div className='contactsDataFoto'>
                <img src={foto} alt={'foto'}/>
            </div>
        </div>
    )
}

export default ContactsData;