import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from '../Footer/Footer.sass';

const Footer = () => {
    return (
        <div className='footer'>
            <div className='footerBlock'>
                <NavLink
                    className='toHome'
                    exact
                    to='/'
                >
                    Новостник
                </NavLink>
                <p>
                    Single Page Application
                </p>
            </div>

            <div className='footerBlock'>
                <p>
                    Дипломный проект
                </p>
            </div>

            <div className='footerBlock'>
                <p>
                    Made by
                </p>
                <h4>
                    Владимир Хобец
                </h4>
            </div>
        </div>
    )
}

export default Footer;