import React from 'react';
import classes from '../NewsDetail/NewsDetail.sass'


const NewsDetail = props => {

    const now = new Date(props.location.state[2]);
    const theMonth = now.getMonth() + 1;
    const theDate = now.getDate();

    const addZero = (param) =>{
        if (param < 10) {
            param = '0' + param;
        };
        return param;
    }

    const finishDate = addZero(theDate)
    const finishMonth = addZero(theMonth)

    const findText = props.location.state[0].split(' ')
    const delLastWord = findText.slice(0, findText.length - 1)
    const delLastWordString = delLastWord.join(' ')
    const lastWord = findText.pop()

    return (
        <div className='newsDetail'>
            <div className='newsDetailInfo'>
                <h2 className='newsDetailTitle'>
                    {delLastWordString} <span style={{color: '#004FEC'}}>{lastWord }</span>
                </h2>

                <a href={props.location.state[5]} target="_blank">
                    {props.location.state[4]}
                </a>
               
                <div className='newsDetailDate'>
                    <div className='myDate'>{finishDate}</div>
                    / 
                    <div className='myMonth'>{finishMonth}</div>
                </div>
            </div>

            <div className='newsDetailData'>
                <div className='newsDetailDataImage'>
                    <img src={props.location.state[3]}></img>
                </div>

                <p className='newsDetailDataText'>
                    {props.location.state[1]}
                </p>
            </div>
        </div>
    )
}

export default NewsDetail;