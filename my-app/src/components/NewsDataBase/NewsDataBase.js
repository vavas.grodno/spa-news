import React, { useEffect, useState } from 'react';
import axios from 'axios';
import NewsData from '../Main/NewsData/NewsData';
import classes from '../NewsDataBase/NewsDataBase.sass';

const  NewsDataList = () => {
    const [data, updateData] = useState(null);
    const requestUrl = 'http://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=26f659a59c8c41c8809510fe2b9bc360';
    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(requestUrl);
            updateData(response.data.articles)

        }   
        fetchData();
    }, [])
    return data;
}



const NewsDataBase = props => {
    const result = NewsDataList()

    let limit = props.countOfNews

    return (
        <div className='NewsDataList'>
             <div className='NewsDataItem'>
                {
                    result && result.map((item, index) => {

                        if (limit > index) {
                            return (
                                <NewsData
                                    key={index}
                                    content={item.content}
                                    title={item.title}
                                    author={item.author}
                                    date={item.publishedAt}
                                    image={item.urlToImage}
                                    url={item.url} 
                                />
                            )
                        } else if (limit == null) {
                            return (
                                <NewsData
                                    key={index}
                                    content={item.content}
                                    title={item.title}
                                    author={item.author}
                                    date={item.publishedAt}
                                    image={item.urlToImage}
                                    url={item.url} 
                                />
                            )
                        }
                    })
                }
             </div>
        </div>
    )
}

export default NewsDataBase;


