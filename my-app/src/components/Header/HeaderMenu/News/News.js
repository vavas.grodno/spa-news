import React from 'react';
import {NavLink} from 'react-router-dom';

const News = () => {
    return (
        <div>
            <NavLink
                exact
                to='/News'
            >
                Новости
            </NavLink>
        </div>
    )
}

export default News;