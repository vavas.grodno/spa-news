import React from 'react';
import {NavLink} from 'react-router-dom';
import classes from '../HeaderMenu/HeaderMenu.sass';

const HeaderMenu = () => {
    return (
        <div className='headerMenu'>
            <ul>
                <li>
                    <NavLink
                        exact
                        to='/'
                    >
                        Главная
                    </NavLink>
                </li>

                <li>
                    <NavLink
                        exact
                        to='/News'
                    >
                        Новости
                    </NavLink>
                </li>

                <li>
                    <NavLink
                        exact
                        to='/Contacts'
                    >
                        Контакты
                    </NavLink>
                </li>
            </ul>
        </div>
    )
}

export default HeaderMenu;