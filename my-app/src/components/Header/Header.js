import React from 'react';
import HeaderMenu from '../Header/HeaderMenu/HeaderMenu';
import { NavLink } from 'react-router-dom';
import classes from '../Header/Header.sass';


const Header = () => {
    return (
        <div className='header'>

            <NavLink
                className='toHome'
                exact
                to='/'
            >
                Новостник
            </NavLink>

            <HeaderMenu/>

            
        </div>
    )
}

export default Header;