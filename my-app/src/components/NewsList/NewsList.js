import React from 'react';
import NewsDataBase from '../NewsDataBase/NewsDataBase';
import classes from '../NewsList/NewsList.sass';

const NewsList = () => {
    return (
        <div className='mainNews'>
            <h1 className='mainNewsTitle'>
                Будь<br></br>в курсе <span>событий</span> 
            </h1>

            <div className='mainNewsBlock'>
                <NewsDataBase
                    countOfNews={9}
                />
            </div>
        </div>
    )
}

export default NewsList;